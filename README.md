# Traffic Trauma Puzzle Solver

This program tries to solve the traffic trauma puzzle game. 

Among several billion combinations there is **only one combination** that **solves the puzzle**.

---

## About the puzzle

The puzzle consists of 16 cards of which each has assigned 4 colours (one color per edge). The puzzle is solved once you managed to arrange them in a 4x4 square. But here is the snag: The cards can only be placed if their edge-colour matches the edge-colours of their neighbouring cards.

---
