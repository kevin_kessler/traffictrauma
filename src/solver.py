import itertools
import os
import multiprocessing
import time
from typing import List, Union

from src.board import Board, \
    BoardIsEmptyError, \
    ForbiddenNeighborError, SlotExceedsDimensionsError
from src.card import Card
from src.stack import Stack


class UselessSolver(object):
    """
    Tries to solve the traffic trauma puzzle by putting cards to the board
    in the same order and orientation as the stack yields them.
    """

    def __init__(self, stack: Stack, board: Board, verbose: bool = False):
        """
        :param stack: the stack of cards that have to be placed on the board
        :param board: the board to which the cards should be placed to
        """
        self.stack = stack
        self.board = board
        self.verbose = verbose

    def solve(self) -> None:
        for card in self.stack:
            self.board.append(card)

        if self.verbose:
            print("UselessSolver: found a solution!")
            print(self.board)


class PermutationOnlySolver(object):
    """
    Tries to solve the traffic trauma puzzle in the same way as the UselessSolver
    by trying to put the given list of cards without turning them.
    However, it tries every possible permutation / order.
    """

    def __init__(self, cards: List[Union[Card, Stack]], board: Board, verbose: bool = False):
        """
        :param cards: a list of cards that have to be placed on the board
        :param board: the board to which the cards should be placed to
        """
        self.board = board
        self.verbose = verbose

        if all(isinstance(x, Stack) for x in cards):
            self.stacks = cards
        else:
            self.stacks = []
            permutations = itertools.permutations(cards)
            for permutation in permutations:
                self.stacks.append(Stack(permutation))

    def solve(self) -> None:
        permutation_count = 0
        solution = None
        for stack in self.stacks:
            permutation_count += 1
            try:
                if self.verbose and permutation_count % 10000 == 0:
                    print("PermutationOnlySolver: Trying to solve permutation %d / %d."
                          % (permutation_count, len(self.stacks)))
                for card in stack:
                    self.board.append(card)
                solution = self.board
                break
            except ForbiddenNeighborError as e:
                self.board.reset()

        if solution is None:
            raise NotSolvableError

        if self.verbose:
            print("PermutationOnlySolver: Found a solution at permutation %d!" % permutation_count)
            print(solution)


parallel_permutation_only_solver_count = None
parallel_permutation_only_fail_count = None
class ParallelPermutationOnlySolver(object):
    """
    Same as PermutationOnlySolver, but tries to solve the different permutations in parallel.
    """

    def __init__(self, cards: List[Card], board: Board, verbose: bool = False):
        """
        :param cards: a list of cards that have to be placed on the board
        :param board: the board to which the cards should be placed to
        """
        self.stacks_parts = [list() for cpu in range(os.cpu_count())]
        self.board = board
        self.verbose = verbose

        global parallel_permutation_only_solver_count
        global parallel_permutation_only_fail_count
        parallel_permutation_only_solver_count = multiprocessing.Value('i', 0)
        parallel_permutation_only_fail_count = multiprocessing.Value('i', 0)

        permutations = itertools.permutations(cards)
        permutation_count = 0
        for permutation in permutations:
            stacks_part_idx = permutation_count % os.cpu_count()
            self.stacks_parts[stacks_part_idx].append(Stack(permutation))
            permutation_count += 1

    def solve(self) -> None:
        pool = multiprocessing.Pool()
        pool.map(self.solve_part, self.stacks_parts)

    def solve_part(self, stacks_part):
        board = Board(self.board.dimension)
        solver = PermutationOnlySolver(stacks_part, board, False)

        global parallel_permutation_only_solver_count
        with parallel_permutation_only_solver_count.get_lock():
            solver.solver_count = parallel_permutation_only_solver_count.value
            parallel_permutation_only_solver_count.value += 1

        try:
            print("ParallelPermutationOnlySolvers: Starting solver %d ... " % solver.solver_count)
            solver.solve()
        except NotSolvableError:
            print("ParallelPermutationOnlySolvers: Solver %d excited without a solution." % solver.solver_count)
            global parallel_permutation_only_fail_count
            with parallel_permutation_only_fail_count.get_lock():
                parallel_permutation_only_fail_count.value += 1
                if parallel_permutation_only_fail_count.value >= os.cpu_count():
                    raise NotSolvableError
        else:
            print("ParallelPermutationOnlySolvers: Solver %d found a solution!\n%s" % (solver.solver_count, solver.board))


class TurnOnlySolver(object):
    """
    Tries to solve the traffic trauma puzzle by putting cards to the board in the same order as the stack yields them.
    It will however try to turn the cards in order to make them fit.
    """

    def __init__(self, stack: Stack, board: Board, verbose: bool = False):
        """
        :param stack: the stack of cards that have to be placed on the board
        :param board: the board to which the cards should be placed to
        """
        self.stack = stack
        self.board = board
        self.verbose = verbose

    def solve(self) -> None:
        for card in self.stack:
            try:
                if card.has_iterated():
                    raise DoesNotFitError

                for card_state in range(4):
                    try:
                        self.board.append(card)
                        break
                    except ForbiddenNeighborError:
                        card.turn()
                        if card.has_iterated():
                            raise DoesNotFitError

            except DoesNotFitError:
                # tried all states of current card in current slot -> does not fit
                # put it back to the stack
                card.reset()
                self.stack.push(card)

                # proceed with previous card in previous slot
                try:
                    previous_card = self.board.pop()
                    previous_card.turn()
                    self.stack.push(previous_card)
                except SlotExceedsDimensionsError:
                    # we have identified that the most upper card of the stack does not fit, hence the puzzle is
                    # not solvable (at least in the order provided by the stack)
                    raise NotSolvableError

        if self.verbose:
            print("TurnOnlySolver: found a solution!")
            print(self.board)


class PuzzleSolver(object):
    """
    Tries to solve the traffic trauma puzzle by trying every possible permutation / order and orientation
    of the given cards. A permutation is discarded as soon as it has proven to be unsolvable.
    """

    def __init__(self, cards: List[Card], board: Board, verbose: bool = False):
        """
        :param cards: a list of cards that have to be placed on the board
        :param board: the board to which the cards should be placed to
        """
        self.stacks = []
        self.board = board
        self.verbose = verbose

        permutations = itertools.permutations(cards)
        for permutation in permutations:
            self.stacks.append(Stack(permutation))

    def solve(self) -> None:
        permutation_count = 0
        solution = None
        for stack in self.stacks:
            permutation_count += 1
            try:
                if self.verbose and permutation_count % 10000 == 0:
                    print("PuzzleSolver: Trying to solve permutation %d / %d." % (permutation_count, len(self.stacks)))
                turn_solver = TurnOnlySolver(stack, self.board)
                turn_solver.solve()
                solution = self.board
                break
            except NotSolvableError:
                self.board.reset()

        if solution is None:
            raise NotSolvableError

        if self.verbose:
            print("PuzzleSolver: found a solution at permutation %d!" % permutation_count)
            print(solution)


class DoesNotFitError(Exception):
    pass


class NotSolvableError(Exception):
    pass
