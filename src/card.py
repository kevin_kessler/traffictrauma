from __future__ import annotations

from typing import Tuple

from src.color import Color


class Card(object):
    """
    Represents playing cards of the traffic trauma puzzle game.
    Each card has four edges of which each edge has a fix color assigned.
    """

    def __init__(self, top_color, right_color, bottom_color, left_color):
        self._colors = (top_color, right_color, bottom_color, left_color)
        self._turns = 0

    @property
    def turns(self):
        return self._turns

    @property
    def iterations(self):
        """
        A card has fully iterated if it has been turned for at least 4 times.
        :return: the iteration count
        """
        return self._turns / 4

    @property
    def state(self) -> int:
        """
        Returns the card's color state, which serves as top_color index.
        The color index depends on the card's turn count.
        There are 4 states, one for each color to indicate which color is currently the top_color:
        0: initial state. not turned or turned four times.
        1: turned once.
        2: turned twice.
        3: turned thrice.
        """
        return -self._turns % 4

    @property
    def _colors(self) -> Tuple[Color]:
        return self.__colors

    @_colors.setter
    @Color.validator
    def _colors(self, colors: Tuple[Color]) -> None:
        assert (len(colors) == 4)
        self.__colors = colors

    @property
    def top_color(self) -> Color:
        """ :returns the color of the card's top edge depending on its current state """
        return self._colors[self.state]

    @property
    def right_color(self) -> Color:
        """ :returns the color of the card's right edge depending on its current state """
        right_idx = (self.state + 1) % 4
        return self._colors[right_idx]

    @property
    def bottom_color(self) -> Color:
        """ :returns the color of the card's bottom edge depending on its current state """
        bottom_idx = (self.state + 2) % 4
        return self._colors[bottom_idx]

    @property
    def left_color(self) -> Color:
        """ :returns the color of the card's bottom edge depending on its current state """
        left_idx = (self.state + 3) % 4
        return self._colors[left_idx]

    def turn(self) -> Card:
        """
        Increments a card's turn count, resulting in a different color state
        """
        self._turns += 1
        return self

    def has_iterated(self) -> bool:
        """
        :return: True if the card has fully iterated at least once (iteration count is at least 1). False otherwise.
        """
        return self.iterations >= 1

    def reset(self) -> Card:
        """ Resets the card to its initial state (turn count = 0) """
        self._turns = 0
        return self

    def is_in_initial_state(self) -> bool:
        """
        :return: True if the card's color state is 0 (never turned or turned 4 times), False otherwise
        """
        return self.state == 0

    def __hash__(self) -> int:
        """
        Two cards have the same hash if they have the same color distribution (independant of the turn state).
        Hence the hash is based on the color distribution, determined by combining the hashs of each permutation.
        :return: the hash value of this card
        """
        if not hasattr(self, "_hash"):
            self._hash = 0
            for i in range(4):
                colors = (self.top_color, self.right_color, self.bottom_color, self.left_color)
                self._hash += hash(colors)
                self.turn()
            self.reset()
        return self._hash

    def __eq__(self, other: Card) -> bool:
        """
        We consider two cards as equal if their color distribution / order is the same (independant of the turn state).
        :param other: Card object to compare
        :return: True if equal. False otherwise.
        """
        if not isinstance(other, Card):
            return False

        return hash(self) == hash(other)

    def color_state_equals(self, other: Card) -> bool:
        """
        More specific than __eq__:
        We consider color state as equal if both cards colors match at the same positions in their current turn state
        :param other: Card object to compare
        :return: True if color state is equal. False otherwise.
        """

        return other.top_color == self.top_color \
            and other.right_color == self.right_color \
            and other.bottom_color == self.bottom_color \
            and other.left_color == self.left_color

    def __repr__(self) -> str:
        """ :returns a human readable str represenation of the card object and its four edges / colors"""
        line_format = '{:7s}{:7s}{:7s}'
        return "%s\n%s\n%s\n" % (self.repr_top(line_format),
                                 self.repr_middle(line_format),
                                 self.repr_bottom(line_format))

    def repr_top(self, line_format: str) -> str:
        """ :returns the top line of the card's str representation including the top edge color """
        return line_format.format('', self.top_color.name, '')

    def repr_middle(self, line_format: str) -> str:
        """ :returns the middle line of the card's str representation including the left and right edge colors """
        return line_format.format(self.left_color.name, '', self.right_color.name)

    def repr_bottom(self, line_format: str) -> str:
        """ :returns the top line of the card's str representation including the bottom edge color """
        return line_format.format('', self.bottom_color.name, '')
