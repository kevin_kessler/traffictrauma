from __future__ import annotations

from typing import Iterable


class Stack(object):
    """
    Simple stack data structure (LIFO)
    """

    def __init__(self, items: Iterable = None):
        if items is None:
            items = []
        self._stack = list(items)

    def push(self, item) -> None:
        self._stack.append(item)

    def pop(self) -> object:
        try:
            return self._stack.pop()
        except IndexError:
            raise StackIsEmptyError("Can not pop from empty stack")

    def is_empty(self) -> bool:
        return len(self) <= 0

    def __len__(self) -> int:
        return len(self._stack)

    def __iter__(self) -> Stack:
        return self

    def __next__(self) -> object:
        try:
            return self.pop()
        except StackIsEmptyError:
            raise StopIteration


class UniqueStack(Stack):
    """
    Simple stack data structure (LIFO) that does not allow duplicates
    """

    def __init__(self, *items):
        self._duplicate_tracker = dict()
        super().__init__(*items)

    def push(self, item) -> None:
        if item in self._duplicate_tracker:
            raise DuplicateInStackError("\n"+str(item))
        self._duplicate_tracker[item] = 1
        super().push(item)

    def pop(self) -> object:
        item = super().pop()
        del(self._duplicate_tracker[item])
        return item


class StackIsEmptyError(Exception):
    pass


class DuplicateInStackError(Exception):
    pass
