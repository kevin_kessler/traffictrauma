from src.board import Board
from src.card import Card
from src.color import Color
from src.solver import PermutationOnlySolver, ParallelPermutationOnlySolver, PuzzleSolver


def permutation_only_solver():
    """
    Runs the PermutationOnlySolver with a 3x3 board and a stack of 9 cards in correct orientation but wrong order for
    benchmark purposes (run with -m cProfile -s cumtime)
    """
    t0 = Card(Color.RED, Color.BLACK, Color.GREEN, Color.PINK)
    t1 = Card(Color.PURPLE, Color.YELLOW, Color.BLACK, Color.BLACK)
    t2 = Card(Color.RED, Color.RED, Color.BLACK, Color.YELLOW)

    t3 = Card(Color.GREEN, Color.RED, Color.GREEN, Color.ORANGE)
    t4 = Card(Color.BLACK, Color.GREEN, Color.RED, Color.RED)
    t5 = Card(Color.BLACK, Color.YELLOW, Color.YELLOW, Color.GREEN)

    t6 = Card(Color.GREEN, Color.RED, Color.YELLOW, Color.BLACK)
    t7 = Card(Color.RED, Color.PURPLE, Color.PURPLE, Color.RED)
    t8 = Card(Color.YELLOW, Color.PURPLE, Color.BLACK, Color.PURPLE)

    cards = [t2, t0, t1, t3, t5, t4, t8, t7, t6]
    board = Board(3)

    solver = PermutationOnlySolver(cards, board, verbose=True)
    solver.solve()


def parallel_permutation_only_solver():
    """
    Runs the ParallelPermutationOnlySolver with a 3x3 board and a stack of 9 cards in correct
    orientation but wrong order for benchmark purposes (run with -m cProfile -s cumtime)
    """
    t0 = Card(Color.RED, Color.BLACK, Color.GREEN, Color.PINK)
    t1 = Card(Color.PURPLE, Color.YELLOW, Color.BLACK, Color.BLACK)
    t2 = Card(Color.RED, Color.RED, Color.BLACK, Color.YELLOW)

    t3 = Card(Color.GREEN, Color.RED, Color.GREEN, Color.ORANGE)
    t4 = Card(Color.BLACK, Color.GREEN, Color.RED, Color.RED)
    t5 = Card(Color.BLACK, Color.YELLOW, Color.YELLOW, Color.GREEN)

    t6 = Card(Color.GREEN, Color.RED, Color.YELLOW, Color.BLACK)
    t7 = Card(Color.RED, Color.PURPLE, Color.PURPLE, Color.RED)
    t8 = Card(Color.YELLOW, Color.PURPLE, Color.BLACK, Color.PURPLE)

    cards = [t2, t0, t1, t3, t5, t4, t8, t7, t6]
    board = Board(3)

    solver = ParallelPermutationOnlySolver(cards, board, verbose=True)
    solver.solve()


def puzzle_solver():
    """
    Runs the PuzzleSolver with a 3x3 board and a stack of 9 cards in wrong orientation and wrong order for
    benchmark purposes (run with -m cProfile -s cumtime)
    """
    t0 = Card(Color.PINK, Color.RED, Color.BLACK, Color.GREEN)
    t1 = Card(Color.BLACK, Color.BLACK, Color.PURPLE, Color.YELLOW)
    t2 = Card(Color.RED, Color.BLACK, Color.YELLOW, Color.RED)

    t3 = Card(Color.GREEN, Color.ORANGE, Color.GREEN, Color.RED)
    t4 = Card(Color.RED, Color.BLACK, Color.GREEN, Color.RED)
    t5 = Card(Color.YELLOW, Color.GREEN, Color.BLACK, Color.YELLOW)

    t6 = Card(Color.BLACK, Color.GREEN, Color.RED, Color.YELLOW)
    t7 = Card(Color.PURPLE, Color.PURPLE, Color.RED, Color.RED)
    t8 = Card(Color.PURPLE, Color.BLACK, Color.PURPLE, Color.YELLOW)

    cards = [t8, t1, t4, t6, t3, t0, t5, t7, t2]
    board = Board(3)

    solver = PuzzleSolver(cards, board, verbose=True)
    solver.solve()


permutation_only_solver()
parallel_permutation_only_solver()
# puzzle_solver()

'''
PermutationOnlySolver: Trying to solve permutation 277076 / 362880.
PermutationOnlySolver: Found a solution!
Using one dimensional matrix for board
23155782 function calls (21934314 primitive calls) in 17.573 seconds
   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
     12/1    0.000    0.000   17.573   17.573 {built-in method builtins.exec}
        1    0.088    0.088   17.573   17.573 benchmark.py:1(<module>)
        1    0.000    0.000   17.443   17.443 benchmark.py:9(permutation_only_solver)
        1    2.468    2.468   14.738   14.738 solver.py:55(solve)
   610690    0.709    0.000    6.403    0.000 board.py:55(append)
   610690    1.817    0.000    5.694    0.000 board.py:141(put)
   610691    0.595    0.000    2.889    0.000 stack.py:31(__next__)
        1    0.239    0.239    2.706    2.706 solver.py:42(__init__)
   362880    1.281    0.000    2.426    0.000 stack.py:9(__init__)
   277078    2.255    0.000    2.256    0.000 {built-in method builtins.print}
   610690    0.553    0.000    1.565    0.000 stack.py:17(pop)
4331742/3110361    0.967    0.000    1.495    0.000 {built-in method builtins.len}
  1943863    1.257    0.000    1.458    0.000 board.py:177(get)
  3265920    0.778    0.000    1.145    0.000 stack.py:14(push)
   610690    0.280    0.000    0.783    0.000 stack.py:22(is_empty)
   340057    0.454    0.000    0.778    0.000 board.py:186(get_north_of)
  1221381    0.528    0.000    0.663    0.000 stack.py:25(__len__)
   325886    0.436    0.000    0.662    0.000 card.py:58(right_color)
   610690    0.411    0.000    0.608    0.000 board.py:207(get_west_of)
   277075    0.230    0.000    0.585    0.000 board.py:214(reset)
   333615    0.270    0.000    0.508    0.000 board.py:193(get_east_of)
'''

'''
PermutationOnlySolver: Trying to solve permutation 277076 / 362880.
PermutationOnlySolver: Found a solution!
Decrease print frequency, improve stack init, next and pop
10183183 function calls (10183096 primitive calls) in 5.974 seconds
   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
     12/1    0.000    0.000    5.974    5.974 {built-in method builtins.exec}
        1    0.058    0.058    5.974    5.974 benchmark.py:1(<module>)
        1    0.000    0.000    5.880    5.880 benchmark.py:9(permutation_only_solver)
        1    0.714    0.714    4.926    4.926 solver.py:55(solve)
   610690    0.317    0.000    3.329    0.000 board.py:55(append)
   610690    1.004    0.000    3.012    0.000 board.py:141(put)
        1    0.258    0.258    0.954    0.954 solver.py:42(__init__)
   362880    0.653    0.000    0.653    0.000 stack.py:11(__init__)
   610691    0.228    0.000    0.554    0.000 stack.py:34(__next__)
  1943863    0.554    0.000    0.554    0.000 board.py:177(get)
   340057    0.223    0.000    0.359    0.000 board.py:186(get_north_of)
   325886    0.219    0.000    0.340    0.000 card.py:58(right_color)
   610690    0.251    0.000    0.334    0.000 board.py:207(get_west_of)
   610691    0.212    0.000    0.327    0.000 stack.py:19(pop)
   325886    0.196    0.000    0.299    0.000 card.py:70(left_color)
   277075    0.114    0.000    0.296    0.000 board.py:214(reset)
   333615    0.187    0.000    0.275    0.000 board.py:193(get_east_of)
   333615    0.139    0.000    0.224    0.000 board.py:200(get_south_of)
   277076    0.182    0.000    0.182    0.000 board.py:12(__init__)
   667778    0.132    0.000    0.132    0.000 card.py:30(state)
'''