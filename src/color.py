from enum import Enum, auto, unique
from functools import wraps


@unique
class Color(Enum):
    """ This class is an enumeration of colors """
    YELLOW = auto()
    GREEN = auto()
    RED = auto()
    PURPLE = auto()
    PINK = auto()
    WHITE = auto()
    BLACK = auto()
    ORANGE = auto()

    @staticmethod
    def validator(method):
        """ Decorator to check a object-method's arguments for valid Color instances """
        @wraps(method)
        def wrapper(self, colors):
            for color in colors:
                if not isinstance(color, Color):
                    raise NotAColorError

            method(self, colors)

        return wrapper


class NotAColorError(Exception):
    pass
