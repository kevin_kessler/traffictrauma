import unittest
from copy import copy
from random import shuffle

from src.board import Board, \
    ForbiddenNeighborInNorthError, \
    ForbiddenNeighborInEastError, \
    ForbiddenNeighborInSouthError, \
    ForbiddenNeighborInWestError, \
    SlotExceedsDimensionsError, \
    SlotOccupiedError
from src.card import Card
from src.color import Color, NotAColorError
from src.stack import Stack, DuplicateInStackError, StackIsEmptyError, UniqueStack
from src.solver import PermutationOnlySolver, PuzzleSolver, UselessSolver, TurnOnlySolver, NotSolvableError, \
    ParallelPermutationOnlySolver


class TestCard(unittest.TestCase):

    def setUp(self) -> None:
        self.card = Card(Color.BLACK, Color.WHITE, Color.RED, Color.GREEN)

    def tearDown(self) -> None:
        pass

    def testColors(self):
        test_color_cases = (["test", Color.WHITE, Color.WHITE, Color.WHITE],
                            [Color.WHITE, -1, Color.WHITE, Color.WHITE],
                            [Color.WHITE, Color.WHITE, None, Color.WHITE],
                            [Color.WHITE, Color.WHITE, Color.WHITE, False])

        for test_color_case in test_color_cases:
            self.assertRaises(NotAColorError, setattr, self.card, "_colors", test_color_case)

    def testEquals(self):
        equal_card = Card(Color.WHITE, Color.RED, Color.GREEN, Color.BLACK)
        self.assertEqual(equal_card, self.card)
        self.assertFalse(equal_card.color_state_equals(self.card))

        color_state_equal_card = Card(Color.BLACK, Color.WHITE, Color.RED, Color.GREEN)
        self.assertEqual(color_state_equal_card, self.card)
        self.assertTrue(color_state_equal_card.color_state_equals(self.card))

    def testTurn(self):
        turned_card = Card(Color.GREEN, Color.BLACK, Color.WHITE, Color.RED)
        self.assertFalse(turned_card.color_state_equals(self.card))
        self.card.turn()
        self.assertTrue(turned_card.color_state_equals(self.card))

        for i in range(4):
            self.card.turn()
        self.assertTrue(turned_card.color_state_equals(self.card))

    def testReset(self):
        cp = copy(self.card)
        cp.turn()
        self.assertFalse(cp.color_state_equals(self.card))
        cp.reset()
        self.assertTrue(cp.color_state_equals(self.card))


class TestBoard(unittest.TestCase):

    def setUp(self) -> None:
        self.board = Board(4)

        # working 2x2 example if appended to 2x2 board in this order
        self.c0 = Card(Color.RED, Color.BLACK, Color.GREEN, Color.PINK)
        self.c1 = Card(Color.PURPLE, Color.YELLOW, Color.BLACK, Color.BLACK)
        self.c2 = Card(Color.GREEN, Color.RED, Color.GREEN, Color.ORANGE)
        self.c3 = Card(Color.BLACK, Color.GREEN, Color.RED, Color.RED)

    def tearDown(self) -> None:
        pass

    def testDimensions(self):
        self.assertEqual(self.board._dimension**2, len(self.board._slots))

    def testPut(self):
        self.board.put(self.c0, self.board.slot_to_index(1, 1))
        self.assertRaises(SlotOccupiedError, self.board.put, self.c1, self.board.slot_to_index(1, 1))

        # check forbidden neighbor placements by placing cards around the already placed c0
        self.assertRaises(ForbiddenNeighborInSouthError, self.board.put, self.c1, self.board.slot_to_index(0, 1))
        self.assertRaises(ForbiddenNeighborInWestError, self.board.put, self.c3, self.board.slot_to_index(1, 2))
        self.assertRaises(ForbiddenNeighborInNorthError, self.board.put, self.c1, self.board.slot_to_index(2, 1))
        self.assertRaises(ForbiddenNeighborInEastError, self.board.put, self.c1, self.board.slot_to_index(1, 0))

        # assert board allows to put cards who match their neighbors color
        self.board.reset()
        self.board.put(self.c0, self.board.slot_to_index(0, 0))
        self.assertEqual(self.c0, self.board.get(self.board.slot_to_index(0, 0)))
        self.board.put(self.c1, self.board.slot_to_index(0, 1))
        self.assertEqual(self.c1, self.board.get(self.board.slot_to_index(0, 1)))
        self.board.put(self.c2, self.board.slot_to_index(1, 0))
        self.assertEqual(self.c2, self.board.get(self.board.slot_to_index(1, 0)))
        self.board.put(self.c3, self.board.slot_to_index(1, 1))
        self.assertEqual(self.c3, self.board.get(self.board.slot_to_index(1, 1)))

    def testAppend(self):
        self.board = Board(2)
        self.board.append(self.c0)
        self.assertEqual(self.c0, self.board.get(self.board.slot_to_index(0, 0)))
        self.board.append(self.c1)
        self.assertEqual(self.c1, self.board.get(self.board.slot_to_index(0, 1)))
        self.board.append(self.c2)
        self.assertEqual(self.c2, self.board.get(self.board.slot_to_index(1, 0)))
        self.board.append(self.c3)
        self.assertEqual(self.c3, self.board.get(self.board.slot_to_index(1, 1)))

        self.assertRaises(SlotExceedsDimensionsError, self.board.append, self.c3)
        self.assertTrue(self.board.is_full())

    def testPop(self):
        self.board = Board(2)

        self.assertRaises(SlotExceedsDimensionsError, self.board.pop)
        self.assertTrue(self.board.is_empty())

        self.board.append(self.c0)
        self.board.append(self.c1)
        self.assertFalse(self.board.is_empty())

        c1 = self.board.pop()
        self.assertEqual(self.c1, c1)

        self.board.pop()
        self.assertTrue(self.board.is_empty())


class TestStack(unittest.TestCase):

    def setUp(self) -> None:
        # working 2x2 example if appended to 2x2 board in this order
        self.c0 = Card(Color.RED, Color.BLACK, Color.GREEN, Color.PINK)
        self.c1 = Card(Color.PURPLE, Color.YELLOW, Color.BLACK, Color.BLACK)
        self.c2 = Card(Color.GREEN, Color.RED, Color.GREEN, Color.ORANGE)
        self.c3 = Card(Color.BLACK, Color.GREEN, Color.RED, Color.RED)

        self.stack = Stack([self.c3, self.c2, self.c1, self.c0])

    def tearDown(self) -> None:
        pass

    def testPop(self):
        """
        Assert pop successfully removes first item from the stack and raises exception on empty stacks
        """
        len_before_pop = len(self.stack)
        c0 = self.stack.pop()
        len_after_pop = len(self.stack)

        self.assertEqual(len_after_pop, len_before_pop - 1)
        self.assertIs(c0, self.c0)

        # can not pop from empty stack
        empty_stack = Stack()
        self.assertRaises(StackIsEmptyError, empty_stack.pop)

    def testPush(self):
        """
        Assert push successfully adds an item to the stack
        """
        stack = Stack()
        len_before_push = len(stack)
        stack.push(self.c0)
        len_after_push = len(stack)
        self.assertEqual(len_after_push, len_before_push + 1)

        # assert items in unique stack are unique
        stack = UniqueStack()
        stack.push(self.c0)
        self.assertRaises(DuplicateInStackError, stack.push, self.c0)

    def testOrder(self):
        """
        Asserts stack behaves like lifo queue (stack)
        """

        stack = Stack([self.c3, self.c0])
        stack.push(self.c2)
        stack.push(self.c1)

        c1 = stack.pop()
        c2 = stack.pop()
        c0 = stack.pop()
        c3 = stack.pop()

        self.assertEqual(c1, self.c1)
        self.assertEqual(c2, self.c2)
        self.assertEqual(c0, self.c0)
        self.assertEqual(c3, self.c3)


class TestSolver(unittest.TestCase):

    def setUp(self) -> None:
        # working 2x2 example if appended to 2x2 board in this order and orientation
        self.c0 = Card(Color.RED, Color.BLACK, Color.GREEN, Color.PINK)
        self.c1 = Card(Color.PURPLE, Color.YELLOW, Color.BLACK, Color.BLACK)
        self.c2 = Card(Color.GREEN, Color.RED, Color.GREEN, Color.ORANGE)
        self.c3 = Card(Color.BLACK, Color.GREEN, Color.RED, Color.RED)

        # working 3x3 example if appended to 3x3 board in this order and orientation
        self.t0 = Card(Color.RED, Color.BLACK, Color.GREEN, Color.PINK)
        self.t1 = Card(Color.PURPLE, Color.YELLOW, Color.BLACK, Color.BLACK)
        self.t2 = Card(Color.RED, Color.RED, Color.BLACK, Color.YELLOW)

        self.t3 = Card(Color.GREEN, Color.RED, Color.GREEN, Color.ORANGE)
        self.t4 = Card(Color.BLACK, Color.GREEN, Color.RED, Color.RED)
        self.t5 = Card(Color.BLACK, Color.YELLOW, Color.YELLOW, Color.GREEN)

        self.t6 = Card(Color.GREEN, Color.RED, Color.YELLOW, Color.BLACK)
        self.t7 = Card(Color.RED, Color.PURPLE, Color.PURPLE, Color.RED)
        self.t8 = Card(Color.YELLOW, Color.PURPLE, Color.BLACK, Color.PURPLE)

        # working 4x4 example if appended to 4x4 board in this order and orientation
        self.q0 = Card(Color.RED, Color.BLACK, Color.GREEN, Color.PINK)
        self.q1 = Card(Color.ORANGE, Color.YELLOW, Color.BLACK, Color.BLACK)
        self.q2 = Card(Color.RED, Color.RED, Color.BLACK, Color.YELLOW)
        self.q3 = Card(Color.PURPLE, Color.PURPLE, Color.PURPLE, Color.RED)

        self.q4 = Card(Color.GREEN, Color.RED, Color.GREEN, Color.ORANGE)
        self.q5 = Card(Color.BLACK, Color.GREEN, Color.RED, Color.RED)
        self.q6 = Card(Color.BLACK, Color.YELLOW, Color.YELLOW, Color.GREEN)
        self.q7 = Card(Color.PURPLE, Color.YELLOW, Color.YELLOW, Color.YELLOW)

        self.q8 = Card(Color.GREEN, Color.RED, Color.YELLOW, Color.ORANGE)
        self.q9 = Card(Color.RED, Color.PURPLE, Color.PURPLE, Color.RED)
        self.q10 = Card(Color.YELLOW, Color.PURPLE, Color.BLACK, Color.PURPLE)
        self.q11 = Card(Color.YELLOW, Color.BLACK, Color.BLACK, Color.PURPLE)

        self.q12 = Card(Color.YELLOW, Color.RED, Color.YELLOW, Color.BLACK)
        self.q13 = Card(Color.PURPLE, Color.ORANGE, Color.PURPLE, Color.RED)
        self.q14 = Card(Color.BLACK, Color.GREEN, Color.BLACK, Color.ORANGE)
        self.q15 = Card(Color.BLACK, Color.BLACK, Color.BLACK, Color.GREEN)

    def testUselessSolver2x2(self):
        """
        Asserts that the UselessSolver is able to solve a working 2x2 example
        """
        stack = Stack([self.c3, self.c2, self.c1, self.c0])
        board = Board(2)

        useless_solver = UselessSolver(stack, board, verbose=True)
        useless_solver.solve()

        # make sure stack is empty (since all cards should be on the board)
        self.assertRaises(StackIsEmptyError, stack.pop)

        # make sure board is fully occupied
        self.assertTrue(board.is_full())

    def testUselessSolver3x3(self):
        """
        Asserts that the UselessSolver is able to solve a working 2x2 example
        """
        stack = Stack([self.t8, self.t7, self.t6, self.t5, self.t4, self.t3, self.t2, self.t1, self.t0])
        board = Board(3)

        useless_solver = UselessSolver(stack, board, verbose=True)
        useless_solver.solve()

        # make sure stack is empty (since all cards should be on the board)
        self.assertRaises(StackIsEmptyError, stack.pop)

        # make sure board is fully occupied
        self.assertTrue(board.is_full())

    def testUselessSolver4x4(self):
        """
        Asserts that the UselessSolver is able to solve a working 4x4 example
        """
        stack = Stack([self.q15, self.q14, self.q13, self.q12,
                      self.q11, self.q10, self.q9, self.q8,
                      self.q7, self.q6, self.q5, self.q4,
                      self.q3, self.q2, self.q1, self.q0])
        board = Board(4)

        useless_solver = UselessSolver(stack, board, verbose=True)
        useless_solver.solve()

        # make sure stack is empty (since all cards should be on the board)
        self.assertRaises(StackIsEmptyError, stack.pop)

        # make sure board is fully occupied
        self.assertTrue(board.is_full())

    def testPermutationOnlySolver2x2(self):
        """
        Asserts that the PermutationOnlySolver is able to solve a 2x2 example that provides cards in the correct
        orientation but in arbitrary order
        """
        cards = [self.c2, self.c1, self.c0, self.c3]
        shuffle(cards)
        board = Board(2)

        permutation_only_solver = PermutationOnlySolver(cards, board, verbose=True)
        permutation_only_solver.solve()

        # make sure board is fully occupied
        self.assertTrue(board.is_full())

    def testPermutationOnlySolver3x3(self):
        """
        Asserts that the PermutationOnlySolver is able to solve a 3x3 example that provides cards in the correct
        orientation but in arbitrary order
        """

        cards = [self.t0, self.t1, self.t2, self.t3, self.t4, self.t5, self.t6, self.t7, self.t8]
        shuffle(cards)
        board = Board(3)

        permutation_only_solver = PermutationOnlySolver(cards, board, verbose=True)
        permutation_only_solver.solve()

        # make sure board is fully occupied
        self.assertTrue(board.is_full())

    def _testPermutationOnlySolver4x4(self):
        """
        Asserts that the PermutationOnlySolver is able to solve a 4x4 example that provides cards in the correct
        orientation but in arbitrary order
        """

        cards = [self.q11, self.q10, self.q13, self.q7,
                 self.q15, self.q14, self.q3, self.q8,
                 self.q12, self.q0, self.q5, self.q4,
                 self.q9, self.q2, self.q1, self.q6]
        shuffle(cards)
        board = Board(4)

        permutation_only_solver = PermutationOnlySolver(cards, board, verbose=True)
        permutation_only_solver.solve()

        # make sure board is fully occupied
        self.assertTrue(board.is_full())

    def testParallelPermutationOnlySolver2x2(self):
        """
        Asserts that the ParallelPermutationOnlySolver is able to solve a 2x2 example that provides cards in the correct
        orientation but in arbitrary order
        """
        board = Board(2)
        unsolvable_card = Card(Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE)
        cards = [self.c0, self.c0, self.c0, unsolvable_card]
        parallel_permutation_only_solver = ParallelPermutationOnlySolver(cards, board, verbose=True)
        self.assertRaises(NotSolvableError, parallel_permutation_only_solver.solve)

        board = Board(2)
        cards = [self.c2, self.c1, self.c0, self.c3]
        shuffle(cards)
        parallel_permutation_only_solver = ParallelPermutationOnlySolver(cards, board, verbose=True)
        parallel_permutation_only_solver.solve()

    def testPermutationOnlySolver2x2Negative(self):
        """
        Asserts that the PermutationOnlySolver detects if a puzzle is not solvable
        """
        c3 = Card(Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE)
        cards = [self.c2, self.c1, self.c0, c3]
        shuffle(cards)
        board = Board(2)

        permutation_only_solver = PermutationOnlySolver(cards, board)
        self.assertRaises(NotSolvableError, permutation_only_solver.solve)

    def testTurnOnlySolver2x2(self):
        """
        Asserts that the TurnOnlySolver is able to solve a 2x2 example that provides cards in the correct order
        but with the wrong orientation
        """
        c0 = Card(self.c0.left_color, self.c0.top_color, self.c0.right_color, self.c0.bottom_color)
        c1 = Card(self.c1.right_color, self.c1.bottom_color, self.c1.left_color, self.c1.top_color)
        c2 = Card(self.c2.bottom_color, self.c2.left_color, self.c2.top_color, self.c2.right_color)
        c3 = Card(self.c3.left_color, self.c3.top_color, self.c3.right_color, self.c3.bottom_color)

        stack = Stack([c3, c2, c1, c0])
        board = Board(2)

        turn_only_solver = TurnOnlySolver(stack, board, verbose=True)
        turn_only_solver.solve()

        # make sure stack is empty (since all cards should be on the board)
        self.assertRaises(StackIsEmptyError, stack.pop)

        # make sure board is fully occupied
        self.assertTrue(board.is_full())

    def testTurnOnlySolver3x3(self):
        """
        Asserts that the TurnOnlySolver is able to solve a 3x3 example that provides cards in the correct order
        but with the wrong orientation
        """
        t0 = Card(self.t0.left_color, self.t0.top_color, self.t0.right_color, self.t0.bottom_color)
        t1 = Card(self.t1.right_color, self.t1.bottom_color, self.t1.left_color, self.t1.top_color)
        t2 = Card(self.t2.bottom_color, self.t2.left_color, self.t2.top_color, self.t2.right_color)
        t3 = Card(self.t3.left_color, self.t3.top_color, self.t3.right_color, self.t3.bottom_color)
        t4 = Card(self.t4.right_color, self.t4.bottom_color, self.t4.left_color, self.t4.top_color)
        t5 = Card(self.t5.bottom_color, self.t5.left_color, self.t5.top_color, self.t5.right_color)
        t6 = Card(self.t6.left_color, self.t6.top_color, self.t6.right_color, self.t6.bottom_color)
        t7 = Card(self.t7.right_color, self.t7.bottom_color, self.t7.left_color, self.t7.top_color)
        t8 = Card(self.t8.bottom_color, self.t8.left_color, self.t8.top_color, self.t8.right_color)

        stack = Stack([t8, t7, t6, t5, t4, t3, t2, t1, t0])
        board = Board(3)

        turn_only_solver = TurnOnlySolver(stack, board, verbose=True)
        turn_only_solver.solve()

        # make sure stack is empty (since all cards should be on the board)
        self.assertRaises(StackIsEmptyError, stack.pop)

        # make sure board is fully occupied
        self.assertTrue(board.is_full())

    def testTurnOnlySolver4x4(self):
        """
        Asserts that the TurnOnlySolver is able to solve a 4x4 example that provides cards in the correct order
        but with the wrong orientation
        """

        q0 = Card(self.q0.left_color, self.q0.top_color, self.q0.right_color, self.q0.bottom_color)
        q1 = Card(self.q1.right_color, self.q1.bottom_color, self.q1.left_color, self.q1.top_color)
        q2 = Card(self.q2.bottom_color, self.q2.left_color, self.q2.top_color, self.q2.right_color)
        q3 = Card(self.q3.left_color, self.q3.top_color, self.q3.right_color, self.q3.bottom_color)
        q4 = Card(self.q4.right_color, self.q4.bottom_color, self.q4.left_color, self.q4.top_color)
        q5 = Card(self.q5.bottom_color, self.q5.left_color, self.q5.top_color, self.q5.right_color)
        q6 = Card(self.q6.left_color, self.q6.top_color, self.q6.right_color, self.q6.bottom_color)
        q7 = Card(self.q7.right_color, self.q7.bottom_color, self.q7.left_color, self.q7.top_color)
        q8 = Card(self.q8.bottom_color, self.q8.left_color, self.q8.top_color, self.q8.right_color)
        q9 = Card(self.q9.left_color, self.q9.top_color, self.q9.right_color, self.q9.bottom_color)
        q10 = Card(self.q10.right_color, self.q10.bottom_color, self.q10.left_color, self.q10.top_color)
        q11 = Card(self.q11.bottom_color, self.q11.left_color, self.q11.top_color, self.q11.right_color)
        q12 = Card(self.q12.left_color, self.q12.top_color, self.q12.right_color, self.q12.bottom_color)
        q13 = Card(self.q13.right_color, self.q13.bottom_color, self.q13.left_color, self.q13.top_color)
        q14 = Card(self.q14.bottom_color, self.q14.left_color, self.q14.top_color, self.q14.right_color)
        q15 = Card(self.q15.left_color, self.q15.top_color, self.q15.right_color, self.q15.bottom_color)

        stack = Stack([q15, q14, q13, q12, q11, q10, q9, q8, q7, q6, q5, q4, q3, q2, q1, q0])
        board = Board(4)

        turn_only_solver = TurnOnlySolver(stack, board, verbose=True)
        turn_only_solver.solve()

        # make sure stack is empty (since all cards should be on the board)
        self.assertRaises(StackIsEmptyError, stack.pop)

        # make sure board is fully occupied
        self.assertTrue(board.is_full())

    def testTurnOnlySolver2x2Negative(self):
        """
        Asserts that the TurnOnlySolver detects if a puzzle is not solvable
        """
        c0 = Card(self.c0.left_color, self.c0.top_color, self.c0.right_color, self.c0.bottom_color)
        c1 = Card(self.c1.right_color, self.c1.bottom_color, self.c1.left_color, self.c1.top_color)
        c2 = Card(self.c2.bottom_color, self.c2.left_color, self.c2.top_color, self.c2.right_color)
        c3 = Card(Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE)

        stack = Stack([c3, c2, c1, c0])
        board = Board(2)

        turn_only_solver = TurnOnlySolver(stack, board)
        self.assertRaises(NotSolvableError, turn_only_solver.solve)

    def testPuzzleSolver2x2(self):
        """
        Asserts that the PuzzleSolver is able to solve a 2x2 example that provides cards
        in arbitrary order and arbitrary orientation
        """
        c0 = Card(self.c0.left_color, self.c0.top_color, self.c0.right_color, self.c0.bottom_color)
        c1 = Card(self.c1.right_color, self.c1.bottom_color, self.c1.left_color, self.c1.top_color)
        c2 = Card(self.c2.bottom_color, self.c2.left_color, self.c2.top_color, self.c2.right_color)
        c3 = Card(self.c3.left_color, self.c3.top_color, self.c3.right_color, self.c3.bottom_color)
        cards = [c0, c1, c2, c3]
        shuffle(cards)
        board = Board(2)

        puzzle_solver = PuzzleSolver(cards, board, verbose=True)
        puzzle_solver.solve()

        # make sure board is fully occupied
        self.assertTrue(board.is_full())

    def _testPuzzleSolver3x3(self):
        """
        Asserts that the PuzzleSolver is able to solve a 3x3 example that provides cards
        in arbitrary order and arbitrary orientation
        """

        t0 = Card(self.t0.left_color, self.t0.top_color, self.t0.right_color, self.t0.bottom_color)
        t1 = Card(self.t1.right_color, self.t1.bottom_color, self.t1.left_color, self.t1.top_color)
        t2 = Card(self.t2.bottom_color, self.t2.left_color, self.t2.top_color, self.t2.right_color)
        t3 = Card(self.t3.left_color, self.t3.top_color, self.t3.right_color, self.t3.bottom_color)
        t4 = Card(self.t4.right_color, self.t4.bottom_color, self.t4.left_color, self.t4.top_color)
        t5 = Card(self.t5.bottom_color, self.t5.left_color, self.t5.top_color, self.t5.right_color)
        t6 = Card(self.t6.left_color, self.t6.top_color, self.t6.right_color, self.t6.bottom_color)
        t7 = Card(self.t7.right_color, self.t7.bottom_color, self.t7.left_color, self.t7.top_color)
        t8 = Card(self.t8.bottom_color, self.t8.left_color, self.t8.top_color, self.t8.right_color)
        cards = [t0, t1, t2, t3, t4, t5, t6, t7, t8]
        shuffle(cards)
        board = Board(3)

        puzzle_solver = PuzzleSolver(cards, board, verbose=True)
        puzzle_solver.solve()

        # make sure board is fully occupied
        self.assertTrue(board.is_full())

    def testPuzzleSolver2x2Negative(self):
        """
        Asserts that the PuzzleSolver detects if a puzzle is not solvable
        """
        c0 = Card(self.c0.left_color, self.c0.top_color, self.c0.right_color, self.c0.bottom_color)
        c1 = Card(self.c1.right_color, self.c1.bottom_color, self.c1.left_color, self.c1.top_color)
        c2 = Card(self.c2.bottom_color, self.c2.left_color, self.c2.top_color, self.c2.right_color)
        c3 = Card(Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE)
        cards = [c0, c1, c2, c3]
        shuffle(cards)
        board = Board(2)

        puzzle_solver = PuzzleSolver(cards, board)
        self.assertRaises(NotSolvableError, puzzle_solver.solve)