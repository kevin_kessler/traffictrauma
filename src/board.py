from typing import Optional, Tuple, List

from src.card import Card


class Board(object):
    """
    Represents the puzzle board of the traffic trauma puzzle game on which the playing cards are meant to be placed.
    The board is of square nature, which means that it consists of x * x slots were cards can be placed.
    """

    def __init__(self, dimension: int) -> None:
        # simulate a x by x slots matrix with a one dimensional array
        self._dimension = dimension
        self._cur_slot_idx = -1
        self._slot_num = dimension * dimension
        self._slots = [None] * self._slot_num

    @property
    def dimension(self):
        return self._dimension

    def __repr__(self) -> str:
        """ :returns a human readable string representation of the board, its cards and their colors
        Example:
        | --------------------- | --------------------- |
        |        RED            |        PURPLE         |
        | PINK          BLACK   | BLACK         YELLOW  |
        |        GREEN          |        BLACK          |
        | --------------------- | --------------------- |
        |        GREEN          |        BLACK          |
        | ORANGE        RED     | RED           GREEN   |
        |        GREEN          |        RED            |
        | --------------------- | --------------------- |
        """
        card_line_format = '{:7s}{:7s}{:7s} | '
        separator_line = '| ' + card_line_format.format('-' * 7, '-' * 7, '-' * 7) * self._dimension

        res = separator_line + '\n'
        for row in range(self._dimension):
            lines = ['| ', '| ', '| ']
            for col in range(self._dimension):
                idx = self.slot_to_index(row, col)
                card = self.get(idx)
                if card is not None:
                    lines[0] += card.repr_top(card_line_format)
                    lines[1] += card.repr_middle(card_line_format)
                    lines[2] += card.repr_bottom(card_line_format)
                else:
                    lines[0] += card_line_format.format('', '', '')
                    lines[1] += card_line_format.format('', ' EMPTY ', '')
                    lines[2] += card_line_format.format('', '', '')

            res += "%s\n%s\n%s\n%s\n" % (lines[0], lines[1], lines[2], separator_line)

        return res

    def append(self, card: Optional[Card]) -> None:
        """
        Same as put(card, idx) but tries to put the card to 'next_slot'.
        Sets the board's current slot to 'next_slot' if successfully appended.
        :raises SlotExceedsDimensionsError if idx of the current_slot exceeds the board's slot number
        """
        self.put(card, self._cur_slot_idx + 1)
        self._cur_slot_idx += 1

    def pop(self) -> Optional[Card]:
        """
        Same as remove(idx) but tries to remove the card from 'current_slot'.
        Sets the board's current slot to 'previous_slot' if successfully removed.
        :return: the card that was removed
        :raises SlotExceedsDimensionsError if idx of the current_slot exceeds the board's slot number
        """
        card = self.remove(self._cur_slot_idx)
        self._cur_slot_idx -= 1
        return card

    def index_to_slot(self, index) -> Tuple[int, int]:
        """
        Translates the given board index to x and y coordinates of the board's slot matrix
        :param index: the index representing one of the board's slots (flattened out, one dimensional)
        :return: the translated x and y
        """
        x = index // self._dimension
        y = index % self._dimension
        return x, y

    def slot_to_index(self, x: int, y: int) -> int:
        """
        Translates the given x and y into a single index used to access the slots in the board's simulated slot matrix.
        Neither x or y should exceed the board's dimension
        :param x: x coordinate of the slot matrix
        :param y: y coordinate of the slot matrix
        :return: the translated slot index
        :raise SlotExceedsDimensionsError: if the x or y exceeds the board's dimension
        """
        if x < 0 or y < 0 or x > self._dimension or y > self._dimension:
            raise SlotExceedsDimensionsError
        return x * self._dimension + y

    def find_first_empty_slot(self) -> int:
        """
        Iterates through the board's slots to find the first empty one.
        :returns slot index of the next empty slot on the board
        :raises BoardIsFullError if no empty slot was found
        """
        for idx in range(self._slot_num):
            if self.get(idx) is None:
                return idx
        raise BoardIsFullError

    def find_last_occupied_slot(self) -> int:
        """
        Iterates backwards through the board's slots to find the last occupied one.
        :returns the slot_idx of the last occupied slot on the board
        """
        for idx in reversed(range(self._slot_num)):
            if self.get(idx) is not None:
                return idx
        raise BoardIsEmptyError

    def is_full(self) -> bool:
        """
        :returns True if the board is fully occupied by cards, False otherwise
        """
        try:
            self.find_first_empty_slot()
        except BoardIsFullError:
            return True
        else:
            return False

    def is_empty(self) -> bool:
        """
        :returns True if the board is empty (all slots are None), False otherwise
        """
        try:
            self.find_last_occupied_slot()
        except BoardIsEmptyError:
            return True
        else:
            return False

    def put(self, card: Optional[Card], slot_idx: int) -> None:
        """
        Puts the given card (or None) on the boards slot[slot_idx].
        :raises
        - SlotOccupiedError: if the slot is already occupied
        - SlotExceedsDimensionsError: if the x or y exceeds the board's dimension
        - ForbiddenNeighborIn[North|East|South|West]Error:
            for any neighboring slot that is occupied by a card whose edge color
            does not match the edge-color of the card to be put
        """

        if card is None:
            self._slots[slot_idx] = None
            return

        if self.get(slot_idx) is not None:
            raise SlotOccupiedError

        west = self.get_west_of(slot_idx)
        if west is not None and west.right_color is not card.left_color:
            raise ForbiddenNeighborInWestError

        north = self.get_north_of(slot_idx)
        if north is not None and north.bottom_color is not card.top_color:
            raise ForbiddenNeighborInNorthError

        east = self.get_east_of(slot_idx)
        if east is not None and east.left_color is not card.right_color:
            raise ForbiddenNeighborInEastError

        south = self.get_south_of(slot_idx)
        if south is not None and south.top_color is not card.bottom_color:
            raise ForbiddenNeighborInSouthError

        self._slots[slot_idx] = card

    def get(self, slot_idx: int) -> Optional[Card]:
        """
        :returns the card found at slot[slot_idx]
        :raise SlotExceedsDimensionsError: if slot_idx exceeds the board's dimension
        """
        if slot_idx < 0 or slot_idx >= self._slot_num:
            raise SlotExceedsDimensionsError
        return self._slots[slot_idx]

    def get_north_of(self, slot_idx: int) -> Optional[Card]:
        """ :returns the card found at the north neighbor of slot[idx] or None if the neighbor does not exist """
        try:
            return self.get(slot_idx - self._dimension)
        except SlotExceedsDimensionsError:
            return None

    def get_east_of(self, slot_idx: int) -> Optional[Card]:
        """ :returns the card found at the east neighbor of slot[idx] or None if the neighbor does not exist """
        try:
            return self.get(slot_idx + 1) if slot_idx + 1 % self._dimension != 0 else None
        except SlotExceedsDimensionsError:
            return None

    def get_south_of(self, slot_idx: int) -> Optional[Card]:
        """ :returns the card found at the south neighbor of slot[idx] or None if the neighbor does not exist """
        try:
            return self.get(slot_idx + self._dimension)
        except SlotExceedsDimensionsError:
            return None

    def get_west_of(self, slot_idx: int) -> Optional[Card]:
        """ :returns the card found at the west neighbor of slot[idx] or None if the neighbor does not exist """
        try:
            return self.get(slot_idx - 1) if slot_idx % self._dimension != 0 else None
        except SlotExceedsDimensionsError:
            return None

    def reset(self) -> None:
        """
        Resets the board to it's initial state
        """
        self.__init__(self._dimension)

    def remove(self, slot_idx: int) -> Optional[Card]:
        """
        Sets slot[slot_idx] to None and returns the card that was previously stored in that slot
        :return: the card that has been removed
        :raise
            - SlotExceedsDimensionsError if slot_idx exceeds the board's slots number
        """
        card = self.get(slot_idx)
        self.put(None, slot_idx)
        return card


class ForbiddenNeighborError(Exception):
    pass


class ForbiddenNeighborInNorthError(ForbiddenNeighborError):
    pass


class ForbiddenNeighborInEastError(ForbiddenNeighborError):
    pass


class ForbiddenNeighborInWestError(ForbiddenNeighborError):
    pass


class ForbiddenNeighborInSouthError(ForbiddenNeighborError):
    pass


class SlotExceedsDimensionsError(IndexError):
    pass


class SlotOccupiedError(Exception):
    pass


class BoardIsFullError(Exception):
    pass


class BoardIsEmptyError(Exception):
    pass
